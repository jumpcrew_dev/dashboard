// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  server_url: 'https://testadmin.jumpcrew.com',
  admin_url: 'http://104.236.168.218/cgi-bin/fire_admin.cgi?env=dev',
  gae_url: 'https://jumptest-41951.appspot.com/?file=',
  thumb_suffix: '=s200-c',
  file_icon: 'http://lh3.googleusercontent.com/oQsVrN_00c5BcmHShNwjfmatMAncxKzzVLkIOOa0oUXq7w8xQDPLWuJGpUZ6B3GsPFkXhu8ExGX4wz8Ri3gyLm0M0rc',
  jumpcrew_api_base: 'https://testapi.jumpcrew.com'
};
export const firebaseConfig = {
    apiKey: "AIzaSyDgY5CzMPLADq3MOJzOtEb6GJKnD4dH350",
    authDomain: "jumptest-41951.firebaseapp.com",
    databaseURL: "https://jumptest-41951.firebaseio.com",
    projectId: "jumptest-41951",
    storageBucket: "jumptest-41951.appspot.com",
    messagingSenderId: "678338956556"
  };
