export const environment = {
  production: true,
  server_url: 'https://admin.jumpcrew.com',
  admin_url: 'http://104.236.168.218/cgi-bin/fire_admin.cgi?env=prod',
  thumb_suffix: '=s200-c',
  gae_url: 'https://jumpcrew-8105e.appspot.com/?file=',
  file_icon: 'http://lh3.googleusercontent.com/oQsVrN_00c5BcmHShNwjfmatMAncxKzzVLkIOOa0oUXq7w8xQDPLWuJGpUZ6B3GsPFkXhu8ExGX4wz8Ri3gyLm0M0rc',
  jumpcrew_api_base: 'https://api.jumpcrew.com'
};
export const firebaseConfig = {
    apiKey: "AIzaSyDuEmdaeM1jCy_wS9taRfJx6hUuyvSNJrs",
    authDomain: "jumpcrew-8105e.firebaseapp.com",
    databaseURL: "https://jumpcrew-8105e.firebaseio.com",
    storageBucket: "jumpcrew-8105e.appspot.com",
    messagingSenderId: "37814297968"
};
