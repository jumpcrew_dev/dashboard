import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserService } from '../services/user.service';
declare var mixpanel: any;
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';
import { CarouselComponent } from "angular2-carousel";
import { OrderByPipe } from '../order-by.pipe';
import { KeysPipe } from '../keys.pipe';
import { MatTabsModule } from '@angular/material/tabs';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  user: FirebaseObjectObservable<any>;
  key: string;
  company: FirebaseObjectObservable<any>;
  reports: FirebaseListObservable<any>;
  url: Observable<string>;
  urlString: string = "";
  date: string;
  fb_page_ids = [];
  fb_day_metrics:{} = {};
  tw_page_ids = [];
  tw_day_metrics:{} = {};
  gp_page_ids = [];
  ig_page_ids = [];
  ig_day_metrics:{} = {};
  chart_data:{};
  tw_fans_data:{};
  tw_activity_data:{};
  ig_fans_data:{};
  ig_activity_data:{};

  pie_data:{};
  gender_data:{};
  data:{} = {};
  options:{};
  pie_options:{};
  cities_options:{};
  top_cities: {};
  type:string;
  orderPipe:OrderByPipe;
  keysPipe: KeysPipe;
  @ViewChild('postsSlider') postsSlider: CarouselComponent;

  constructor( public db: AngularFireDatabase, private afAuth:AngularFireAuth,
    private userService:UserService ) {
      this.orderPipe = new OrderByPipe();
      this.keysPipe = new KeysPipe();

      this.url = Observable.of(this.urlString);
      this.date = moment().subtract(1, 'days').format("YYYY-MM-DD");
     //console.log(this.date);

      this.type = 'line';
      this.options = {
            legend: {
              position: 'bottom',
              fullWidth: true,
              labels: {
                fontSize: 16
              }
            },
            responsive: true,
            maintainAspectRatio: false,
            layout: {
              height: 100
            }
        };

        this.pie_options = {
            legend: {
              position: 'bottom',
              fullWidth: true,
              labels: {
                fontSize: 16
              }
            },
            responsive: true,
            maintainAspectRatio: false,
            layout: {
              height: 100
            }
        };

        this.cities_options = {
           legend: {
              display: false
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        fontSize: 12
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 12
                    }
                }]

            }

        }

    }

  ngOnInit() {
      window.scrollTo(0, 0);
      mixpanel.track("Reporting Page");
    this.userService.isLoaded().subscribe( loaded => {
    if ( loaded ){
       this.userService.getCurrentUser().subscribe(
         snapshot => {
           this.company = this.userService.getCurrentAccount();
           this.company.subscribe(
             obj => {
                this.key = obj.$key;
                if ( obj.networks ){
                 //console.log(obj.networks);
                  this.reports = this.db.list("/reports/" + this.key,
                    {
                      query: {
                        orderByKey: true,
                        limitToLast: 1,
                      }
                    }
                  );
                  this.reports.subscribe(
                     reports => {
                      //console.log(reports);
                      this.chart_data = null;
                      this.pie_data = null;
                      this.gender_data = null;
                       if ( reports.length > 0 ){
                        if ( reports[0].fb_pages ){
                         this.fb_page_ids = Object.keys(reports[0].fb_pages);
                         this.getFBPieChartData(reports[0].fb_pages);
                         this.getFBGenderChartData(reports[0].fb_pages);
                         this.getFBDayMetrics(reports[0].start_date, reports[0].end_date);
                         this.getFBTopCities(reports[0].fb_pages);
                        }
                        if ( reports[0].tw_pages ){
                         this.tw_page_ids = Object.keys(reports[0].tw_pages);
                         this.getTWDayMetrics(reports[0].start_date, reports[0].end_date);
                        }
                        if ( reports[0].gp_pages ){
                         this.gp_page_ids = Object.keys(reports[0].gp_pages);
                         //this.getTWDayMetrics(reports[0].start_date, reports[0].end_date);
                        }
                        if ( reports[0].ig_pages ){
                         this.ig_page_ids = Object.keys(reports[0].ig_pages);
                         this.getIGDayMetrics(reports[0].start_date, reports[0].end_date);
                        }
                       }
                     }

                   );
                }
               }
             );
         }
       );
     }
     });
  }

  getFBTopCities(pages){
    this.top_cities = {};
    this.fb_page_ids.forEach(
      pageID => {
        if ( pages[pageID].location_data && pages[pageID].location_data.page_fans_city ){
           let chartData = {};
           let cities = pages[pageID].location_data.page_fans_city;
           let labels = [];
           let data = [];
           cities = this.orderPipe.transform(this.keysPipe.transform(cities, null), ['-value']);
            for ( let i = 0; i < 5; i++ ){
                labels.push(cities[i]['key']);
                data.push(cities[i]['value']);
            }
           chartData['labels'] = labels;
           chartData['datasets'] = new Array();
           chartData['datasets'].push({'data':data, 'backgroundColor': ['#ED682A','#275E99','#15B558','#565554','#7C9EB2']});
          //console.log(chartData);
          this.top_cities[pageID] = chartData;
        }
      }
    );

  }

  getFBGenderChartData(pages){
    this.gender_data = {};
    this.fb_page_ids.forEach(
      pageID => {
        if ( pages[pageID].gender_metrics && pages[pageID].gender_metrics.men && pages[pageID].gender_metrics.men.fans ){
          let men = pages[pageID].gender_metrics.men.fans;
          let women = pages[pageID].gender_metrics.women.fans;
          let chartData = {};
          let keys = Object.keys(men);
          let mdata = [];
          let wdata = [];
          let labels = [];
          keys.forEach(
            label => {
              if ( label != 'total' ){
               labels.push(label);
               mdata.push(men[label]);
               wdata.push(women[label]);
              }
            });
          chartData['labels'] = labels;
          chartData['datasets'] = new Array();
          chartData['datasets'].push({'label':"Women", 'data':wdata, 'backgroundColor': '#669EFD'});
          chartData['datasets'].push({'label':"Men", 'data':mdata, 'backgroundColor': '#2C4870'});

         //console.log(chartData);
          this.gender_data[pageID] = chartData;
        }
    });

  }


  getFBPieChartData(pages){
    this.pie_data = {};
    this.fb_page_ids.forEach(
      pageID => {
        if ( pages[pageID].like_sources ){
          let map = pages[pageID].like_sources;
          let chartData = {};
          let keys = Object.keys(map);
          let data = [];
          let labels = [];
          keys.forEach(
            label => {
               labels.push(label.charAt(0).toUpperCase() + label.slice(1).replace(/_/g," "));
               data.push(map[label]);
            });
          chartData['labels'] = labels;
          chartData['datasets'] = new Array();
          chartData['datasets'].push({'data':data, 'backgroundColor': ['#ED682A','#15B558','#565554','#7C9EB2','#275E99']});

         //console.log(chartData);
          this.pie_data[pageID] = chartData;
        }
    });

  }

  getFBDayMetrics(start, end){
    this.fb_day_metrics = {};
    this.fb_page_ids.forEach(
      fb_page_id => {
        let path = "/day_metrics/" + this.key + "/fb_pages/" + fb_page_id;
        let list = this.db.list(path, {
                      query: {
                       orderByKey: true,
                       startAt: start,
                       endAt: end
                     }
                    });
        list.subscribe(
          data => {
            let labels = new Array();
            this.chart_data = {};
            let organic = {};
            organic['data'] = new Array();
            organic['label'] = "Organic Impressions";
            organic['backgroundColor'] = '#2C4870';

            let paid = {};
            paid['data'] = new Array();
            paid['label'] = "Paid Impressions";
            paid['backgroundColor'] = '#669EFD';

            let fans = {};
            fans['data'] = new Array();
            fans['label'] = "Fans";
            fans['backgroundColor'] = '#2C4870';

             this.chart_data['datasets'] = new Array();

            data.forEach(datum => {
              labels.push( datum.$key );
              organic['data'].push( datum.page_impressions_organic_unique_day );
              paid['data'].push( datum.page_impressions_paid_unique_day );
              fans['data'].push( datum.page_fans_day );
            });

             this.chart_data['labels'] = labels;
             this.chart_data['datasets'].push(organic);
             this.chart_data['datasets'].push(paid);
             //this.chart_data['datasets'].push(fans);
            //console.log(this.chart_data);
            //console.log(this.data);
          });
        this.fb_day_metrics[fb_page_id] = list;

      }
    );
  }

  getTWDayMetrics(start, end){
    this.tw_day_metrics = {};
    this.tw_page_ids.forEach(
      tw_page_id => {
        let path = "/day_metrics/" + this.key + "/tw_pages/" + tw_page_id;
        let list = this.db.list(path, {
                      query: {
                       orderByKey: true,
                       startAt: start,
                       endAt: end
                     }
                    });
        list.subscribe(
          data => {
            this.tw_fans_data = {};
            this.tw_fans_data['datasets'] = new Array();

            this.tw_activity_data = {};
            this.tw_activity_data['datasets'] = new Array();

            let labels = new Array();

            let retweets = {};
            retweets['data'] = new Array();
            retweets['label'] = "Retweets";
            retweets['backgroundColor'] = '#2C4870';

            let mentions = {};
            mentions['data'] = new Array();
            mentions['label'] = "Mentions";
            mentions['backgroundColor'] = '#669EFD';

            let fans = {};
            fans['data'] = new Array();
            fans['label'] = "Followers";
            fans['backgroundColor'] = '#2C4870';

            data.forEach(datum => {
              labels.push( datum.$key );
              retweets['data'].push( datum.retweets );
              mentions['data'].push( datum.mentions );
              fans['data'].push( datum.followers );
            });

            this.tw_fans_data['labels'] = labels;
            this.tw_fans_data['datasets'].push(fans);

            this.tw_activity_data['labels'] = labels;
            this.tw_activity_data['datasets'].push(retweets);
            this.tw_activity_data['datasets'].push(mentions);
          });
        this.tw_day_metrics[tw_page_id] = list;

      }
    );
  }

  getIGDayMetrics(start, end){
    this.ig_day_metrics = {};
    this.ig_page_ids.forEach(
      ig_page_id => {
        let path = "/day_metrics/" + this.key + "/ig_pages/" + ig_page_id;
        let list = this.db.list(path, {
                      query: {
                       orderByKey: true,
                       startAt: start,
                       endAt: end
                     }
                    });
        list.subscribe(
          data => {
            this.ig_fans_data = {};
            this.ig_fans_data['datasets'] = new Array();

            this.ig_activity_data = {};
            this.ig_activity_data['datasets'] = new Array();

            let labels = new Array();

            let reach = {};
            reach['data'] = new Array();
            reach['label'] = "Reach";
            reach['backgroundColor'] = '#2C4870';

            let impressions = {};
            impressions['data'] = new Array();
            impressions['label'] = "Impressions";
            impressions['backgroundColor'] = '#669EFD';

            let engagement = {};
            engagement['data'] = new Array();
            engagement['label'] = "Engaged Users";
            engagement['backgroundColor'] = 'yellow';


            let fans = {};
            fans['data'] = new Array();
            fans['label'] = "Followers";
            fans['backgroundColor'] = '#2C4870';

            data.forEach(datum => {
              labels.push( datum.$key );
              reach['data'].push( datum.reach );
              impressions['data'].push( datum.impressions );
              fans['data'].push( datum.followers );
              engagement['data'].push( datum.engagements );
            });

            this.ig_fans_data['labels'] = labels;
            this.ig_fans_data['datasets'].push(fans);


            this.ig_activity_data['labels'] = labels;
            this.ig_activity_data['datasets'].push(reach);
            this.ig_activity_data['datasets'].push(impressions);
            //this.ig_activity_data['datasets'].push(engagement);

            //console.log(this.ig_activity_data);
          });
        this.ig_day_metrics[ig_page_id] = list;

      }
    );
  }

  getEmbedLink(url:string){
    let link = "https://www.facebook.com/plugins/post.php?href=";
    link += encodeURIComponent(url);
    link += "&width=500&show_text=true&appId=1905498349676722&height=659";
    return link;
  }

  prevPost(){
    this.postsSlider.slidePrev();
  }

  nextPost(){
    this.postsSlider.slideNext();
  }

}
