import { Component, OnInit, ViewChild } from '@angular/core';
import { Modal, ModalModule } from "ngx-modal";
import { SociService } from '../services/soci.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'post-modal',
  templateUrl: './post-modal.component.html',
  styleUrls: ['./post-modal.component.css']
})
export class PostModalComponent implements OnInit {
	@ViewChild('postDetails')
	postDetails: any;
	post: any;
	reason: string;
	key: string;
	errorText: string;

	open(){

	}

  constructor( private sociService:SociService, private userService:UserService ) {
   	this.userService.isLoaded().subscribe(
	      loaded => {
	        if ( loaded ){
		         this.userService.getCurrentUser().subscribe(
		           snapshot => {
			             this.userService.getCurrentAccount().subscribe(
			              	obj => {
			             		this.key = obj['$key'];
		                 });
		            });
	        }
	      }
      );
   }

  ngOnInit() {
  }

    editPost(post:any){
    //console.log("Showing details for post: '"+id+"'");
    //console.log(post);
    this.post = post;
    //console.log(this.post);

    if ( this.post.video ){
      setTimeout( () => {
       var elem = document.getElementById("videoElem");
       if ( elem ){
         elem.setAttribute("src",this.post.video.url);
       }
      },500);
    }

    this.postDetails.open();

  };

  modalClosed(){
    var elem = document.getElementById("videoElem");
    if ( elem ){
      elem.setAttribute("src","");
    }
  }

  modify():boolean{
    if ( !this.reason || this.reason == "" ){
      alert("Please let us know what you would like us to change");
      return false;
    }
    //this.post.customer_approved = '-1';
    this.post.rejection_message = this.reason;
    this.sociService.reject(this.key, this.post, this.reason)
      .subscribe( (res) => {
        console.log("Result of reject", res);
        //postDetails.close();
        //return true;
      });
    return true;
  }


  approve(){
    //console.log("Approve");
    this.post.customer_approved = '1';
    this.sociService.approve(this.key, this.post)
      .subscribe( (res) => {
        console.log("Result of approve", res);
      });

  }


}
