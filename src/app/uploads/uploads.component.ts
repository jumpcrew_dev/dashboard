import { Component, OnInit } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { moveIn, fallIn, moveInLeft } from '../router.animations';
import * as firebase from 'firebase';
import { Modal, ModalModule } from "ngx-modal";
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { FileUploader } from 'ng2-file-upload';
import { DomSanitizer } from '@angular/platform-browser';
import { ViewChild }  from '@angular/core';
import { environment } from '../../environments/environment';

import { Subject, Observable } from 'rxjs/Rx';

import { SlackService } from '../services/slack.service';
import { FirebaseAdminService } from '../services/firebase-admin.service';
import { UserService } from '../services/user.service';
import { ImageModalComponent } from '../image-modal/image-modal.component';

//declare var firebase: any;
declare var mixpanel: any;

@Component({
  selector: 'app-uploads',
  templateUrl: './uploads.component.html',
  styleUrls: ['./uploads.component.css']
})
export class UploadsComponent implements OnInit {
  @ViewChild('fileModal') fileModal: any;
  @ViewChild('editModal') editModal: ImageModalComponent;
  images: Subject<FirebaseListObservable<any>>;
  imagesList: FirebaseListObservable<any>;
  key: string;
  user: FirebaseObjectObservable<any>;
  company: FirebaseObjectObservable<any>;
  config: FirebaseObjectObservable<any>;
  domain: string;
  description : string;
  new_description : string;
  errorText: string;
  userName: string;
  p: number = 1;
  slackToken: string;
  channel_name: string;
  selectedImage: Object;
  uploading: boolean;
  pageSize: number = 10;
  imagesLength: number;
  spinner: string = '/assets/spinner.gif';
  appConfig: any;
  userSubscription: any;
  accountSubscription: any;

  constructor( public http: Http, public db: AngularFireDatabase, private afAuth:AngularFireAuth,
          private route: ActivatedRoute, private router: Router, private sanitizer:DomSanitizer,
          private slackService:SlackService, private firebaseAdminService:FirebaseAdminService,
          private userService:UserService ) {

    this.images = new Subject<FirebaseListObservable<any>>();
    this.afAuth.authState.subscribe(auth => {
     if ( auth ){

       this.appConfig = this.db.object("/app_config/").subscribe(
         config => {
   //         console.log(config);
            this.slackToken = config.slack_token;
   //         console.log(this.slackToken);
          }
       );
     }
    });
  }

  sanitize(url:string){
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  ngOnDestroy() {
    //this.userSubscription.unsubscribe();
    //console.log("Unsubscribed from user object");
    //this.accountSubscription.unsubscribe();
    //console.log("Unsubscribed from account object");
    //this.appConfig.unsubscribe();
  }

  ngOnInit() {
    this.errorText = "";
    let storage = firebase.storage();

    mixpanel.track("View Uploads");

    this.userService.isLoaded().subscribe(
      loaded => {
        if ( loaded ){
//          console.log("Loaded");
         this.userSubscription = this.userService.getCurrentUser().subscribe(
           snapshot => {
             this.userName = snapshot['firstName'] + " " + snapshot['lastName'];
              console.log(this.userName);
            this.accountSubscription = this.userService.getCurrentAccount().subscribe(
              obj => {
                   this.key = obj['$key'];
                   console.log(this.key);
                  this.imagesList = this.userService.getUploads();
                   this.imagesList.subscribe(this.images);

                this.domain = obj['domain'];
                 this.channel_name = obj['channel_name'];
              });


           }
         );
        }
     });



  }

  editImage(image){
    this.editModal.editImage(image);
    //var textField = (<HTMLInputElement>document.getElementById('description'));
    //textField.value = image.description;

  }

  isVideo(imageURL:string){
    return /\.(mp4|mov|m4v)/i.test(imageURL);
  }

  saveChanges(){
    console.log(this.selectedImage);
    var textField = (<HTMLInputElement>document.getElementById('description'));
    console.log(textField);
    this.description = textField.value;
    this.selectedImage['description'] = this.description;
    this.imagesList.update(this.selectedImage['$key'], {"description": this.description});
    /*.subscribe( (result) => {
        textField.value = "";
      });*/
        mixpanel.track("Edited Image Description");

    return true;
  }

  upload() {
      var textField = (<HTMLInputElement>document.getElementById('new_description'));
      //console.log(textField);
      this.description = textField.value;
      console.log(this.description);
      if ( !this.description || this.description == "" ) {
        this.errorText =  "Please enter a description of this image";
      }
      else if ( (<HTMLInputElement>document.getElementById('file')).files.length == 0 ){
        this.errorText = "Please choose a file";
      }
      else {
         this.uploading = true;
         // Create a root reference
         let storageRef = firebase.storage().ref();
         console.log((<HTMLInputElement>document.getElementById('file')).files);
         let success = false;
         // This currently only grabs item 0, TODO refactor it to grab them all
         for (let selectedFile of [(<HTMLInputElement>document.getElementById('file')).files[0]]) {
             //console.log(selectedFile);
             // Make local copies of services because "this" will be clobbered
             let path = `/${this.domain}/images/${selectedFile.name}`;
             var iRef = storageRef.child(path);

             var file = selectedFile;
             var thumb = "";
            console.log("File type: "+file.type);
             if ( file.type.includes('video') ){
              //console.log("Video, doing the thumbnail");
              var fileReader = new FileReader();
              fileReader.onload = function() {
               var blob = new Blob([fileReader.result], {type: file.type});
               var url = URL.createObjectURL(blob);
               var video = document.createElement('video');
               var timeupdate = function() {
                 if (snapImage()) {
                   video.removeEventListener('timeupdate', timeupdate);
                   video.pause();
                 }
               };
               video.addEventListener('loadeddata', function() {
                 if (snapImage()) {
                   video.removeEventListener('timeupdate', timeupdate);
                 }
               });
               var snapImage = function() {
                 var canvas = document.createElement('canvas');
                 canvas.width = video.videoWidth;
                 canvas.height = video.videoHeight;
                 canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
                 var image = canvas.toDataURL();
                 var success = image.length > 100000;
                 if (success) {
                    thumb = image;
                   //var img = document.createElement('img');
                   //img.src = image;
                   //document.getElementsByTagName('div')[0].appendChild(img);
                   URL.revokeObjectURL(url);
                 }
                 return success;
               };
               video.addEventListener('timeupdate', timeupdate);
               video.preload = 'metadata';
               video.src = url;
               // Load video in Safari / IE11
               video.muted = true;
               //video.playsInline = true;
               video.play();
             };
             fileReader.readAsArrayBuffer(file);
            }
             iRef.put(selectedFile).then(
                 data => {
                    console.log(data);
                    //  return;
                    var downloadURL = data.downloadURL;
                    var path = data.metadata.fullPath;
                    let image = {};
                    image['url'] = downloadURL;
                    image['path'] = path;
                    image['description'] = this.description;
                    image['uploadedBy'] = this.userName;
                    image['date_uploaded'] = new Date().toISOString();
                    if ( thumb != "" ){
                     image['thumb'] = thumb;
                    }
                    console.log(image);
                    this.db.list(`/companies/${this.key}/images/`).push(image)
                     .then( (img) => {
                       console.log("Image added: ");
                       console.log(img);
                       console.log("Key: "+img.key);
                       if ( thumb == "" ){
                         console.log("Generating Firebase thumbnail");
                         this.firebaseAdminService.getMagicURL(path).subscribe(
                          (magicURL) => {
                          console.log(magicURL);
                          if ( magicURL.indexOf("http") != 0 ){
                            console.log("Error generating magic URL");
                            console.log(magicURL);
                            magicURL = environment.file_icon;
                          }
                            var thumb = magicURL+environment.thumb_suffix;
                           this.db.object(`/companies/${this.key}/images/${img.key}`).update(
                             { 'thumb': thumb, 'magic_url': magicURL }
                           );
                         });
                       }
                       else {
                        let myPath = `/${this.domain}/images/${new Date().toISOString()}.png`;
                        let thumbRef = storageRef.child(myPath);
                        thumbRef.putString(thumb, 'data_url').then((snapshot) => {
                          var frame_url = snapshot.downloadURL;
                          var frame_path = snapshot.metadata.fullPath;
                          this.firebaseAdminService.getMagicURL(frame_path).subscribe(
                            frame_magic_url => {
                              var frame_thumb = frame_magic_url + environment.thumb_suffix;
                              this.db.object(`/companies/${this.key}/images/${img.key}`).update(
                                { 'thumb': frame_thumb, 'frame_magic_url': frame_magic_url, 'frame_url': frame_url }
                              );
                            }
                          );

                        });

                       }
                     });




                           let message = {};
                           message['message'] = this.userName + " uploaded a file";
                           message['attachments'] = [{'image_url': downloadURL,
                                          'title_link': downloadURL,
                                          'title' : 'Click here to download this '+(this.isVideo(image['url']) ? 'video':'image'),
                                          'fields': [
                                            {'title':'Description: ','value':this.description}
                                           ]
                                        }];

                            this.slackService.sendMessage(message, this.channel_name);
                            mixpanel.track("Uploaded Image");
                            this.fileModal.close();
                            this.uploading = false;
                            (<HTMLInputElement>document.getElementById('file')).value = null;
                            textField.value = "";


                 },
                 error => console.log(error)
             );


             //});
         }


       // this.fileModal.hide();


      }



    }

    private handleError (error: Response | any) {
      console.log("In handleError");
      // In a real world app, we might use a remote logging infrastructure
      let errMsg: string;
      if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      console.error(errMsg);
      return Promise.reject(errMsg);
    }

}
