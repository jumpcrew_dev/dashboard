import { Component, OnInit, HostBinding } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { moveIn } from '../router.animations';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [moveIn()],
  host: {'[@moveIn]': ''}
})
export class LoginComponent implements OnInit {
  error: any;

  constructor( public db: AngularFireDatabase, private afAuth:AngularFireAuth, private router: Router) {
      this.afAuth.authState.subscribe(auth => {
      if(auth) {
        this.router.navigateByUrl('/home');
      }
    });
  }

  loginFb() {
    this.afAuth.auth.signInWithPopup(
      new firebase.auth.FacebookAuthProvider(),
    ).then(
        (success) => {
        this.router.navigate(['/uploads']);
      }).catch(
        (err) => {
        this.error = err;
      })
  }
/*
  loginGoogle() {
    this.af.auth.login({
      provider: AuthProviders.Google,
      method: AuthMethods.Popup,
    }).then(
        (success) => {
        this.router.navigate(['/uploads']);
      }).catch(
        (err) => {
        this.error = err;
      })
  }*/

  ngOnInit() {
  }

}
