import { Component, OnInit } from '@angular/core';
import { ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { SociService } from '../services/soci.service';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import 'rxjs/add/operator/take';
import { Observable } from 'rxjs/Rx';
import * as moment from 'moment';
import { UserService } from '../services/user.service';

import { PostModalComponent } from '../post-modal/post-modal.component';

@Component({
  selector: 'app-content-calendar',
  templateUrl: './content-calendar.component.html',
  styleUrls: ['./content-calendar.component.css']
})
export class ContentCalendarComponent implements OnInit {
  @ViewChild('postModal') postModal: PostModalComponent;
  calendar: Array<any>;
  weekDays: Array<string>;
  monthNames: Array<string>;
  month: number;
  year: number;
  contentByDate: any = {};
  contentByID: any;
  company: FirebaseObjectObservable<any>;
  soci_id: string;
  key: string;
  user: FirebaseObjectObservable<any>;
  post: any;
  reason: string;

  constructor( private sociService:SociService,
               private db:AngularFireDatabase,
               private afAuth:AngularFireAuth,
               private userService:UserService ) {

    this.contentByDate = {};
    var today = new Date();
    console.log(today);
    this.month = today.getMonth()+1;
    console.log(this.month);
    this.year = today.getFullYear();

    this.weekDays = ["Sun", "Mon", "Tues", "Wed", "Thu", "Fri", "Sat"];

    this.monthNames = [
      "",
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];
  };

  editPost(post:any){
    this.postModal.editPost(post);
  }

  modalClosed(){
    var elem = document.getElementById("videoElem");
    if ( elem ){
      elem.setAttribute("src","");
    }
  }

  modify():boolean{
    if ( !this.reason || this.reason == "" ){
      alert("Please let us know what you would like us to change");
      return false;
    }
    //this.post.customer_approved = '-1';
    this.post.rejection_message = this.reason;
    this.sociService.reject(this.key, this.post, this.reason)
      .subscribe( (res) => {
        console.log("Result of reject", res);
        //postDetails.close();
        //return true;
      });
    return true;
  }

  approve(){
    //console.log("Approve");
    this.post.customer_approved = '1';
    this.sociService.approve(this.key, this.post)
      .subscribe( (res) => {
        console.log("Result of approve", res);
      });

  }

  prevMonth(){
    this.month--;
    if ( this.month < 1 ){
      this.month = 12;
      this.year--;
    }
    this.createCalendar();
  }

  nextMonth(){
    this.month++;
    if ( this.month > 12 ){
      this.month = 1;
      this.year++;
    }
    this.createCalendar();
  }


  createCalendar(){
    //console.log("Creating calendar");
    this.calendar = new Array();
    for ( var i = 0; i < 6; i++){
      this.calendar[i] = new Array();
      for ( var j = 0; j < 7; j++ ){
        this.calendar[i][j] = {};
      }
    }

    var today = moment();
    var date = moment(new Date(this.month+"/01/"+this.year));
    var weekOfMonth = 0;
    for ( var d = 0; d <= 31; d++ ){
      var dayOfWeek = date.day();
      //console.log(date,dayOfWeek);
      this.calendar[weekOfMonth][dayOfWeek]['dayOfMonth'] = date.date();
      this.calendar[weekOfMonth][dayOfWeek]['date'] = date.format('YYYY-MM-DD');
      if ( today.isSame(date,'day') ){
        this.calendar[weekOfMonth][dayOfWeek]['isToday'] = true;
      }


      weekOfMonth += Math.floor(dayOfWeek/6);

      date.add(1, 'days');

      if ( date.date() == 1 ){
        break;
      }

    }

    //console.log(this.calendar);

  }

  ngOnInit() {
    console.log("In ngOnInit");
    this.userService.isLoaded().subscribe( loaded => {
    if ( loaded ){
      this.userService.getCurrentUser().subscribe(
        snapshot => {
          console.log("Fetched user");
          this.userService.getCurrentAccount().subscribe(
            obj => {
              console.log("Fetched account");
              this.contentByDate = {};
             this.key = obj['$key'];
              console.log(this.key);
             this.soci_id = obj['soci_id'];
              let posts = this.db.list("/companies/" + this.key + "/posts", {
                query: {
                  orderByChild: 'date'
                }
              });

              var today = moment();
              posts.subscribe(
                posts => {
                  console.log("Calendar got event");
                  posts.forEach( post => {

                    var dateStr = post.date_str;
                    if ( !dateStr ){
                      var date = moment(post.schedule);
                      dateStr = date.format('YYYY-MM-DD');
                      //console.log(post);
                    }
                    if ( moment(dateStr).isBefore(today) ){
                      post.expired = true;
                    }
                    if ( !this.contentByDate[dateStr] ){
                      this.contentByDate[dateStr] = new Array();
                    }
                    var found = false;
                    for ( var i = 0; i < this.contentByDate[dateStr].length; i++ ){
                      if ( this.contentByDate[dateStr][i]['id'] == post['id'] ){
                        found = true;
                        this.contentByDate[dateStr][i] = post;
                        break;
                      }
                    }
                    if ( !found ){
                      this.contentByDate[dateStr].push(post);
                    }
                  })
                }
              );
          });
        });

    }
    });

    this.createCalendar();
  }

}
