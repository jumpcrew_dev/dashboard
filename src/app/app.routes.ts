import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './auth.service';
import { SignupComponent } from './signup/signup.component';
import { EmailComponent } from './email/email.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { UploadsComponent } from './uploads/uploads.component';
import { ApproveComponent } from './approve/approve.component';
import { ReportComponent } from './report/report.component';
import { ChatComponent } from './chat/chat.component';
import { ContentCalendarComponent } from './content-calendar/content-calendar.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RequestVideoComponent } from './request-video/request-video.component';

export const router: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'signup', component: SignupComponent },
    { path: 'login-email', component: EmailComponent },
    { path: 'password-reset', component: PasswordResetComponent},
    { path: 'forgot-password', component: ForgotPasswordComponent},
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'uploads', component: UploadsComponent, canActivate: [AuthGuard] },
    { path: 'approve', component: ApproveComponent, canActivate: [AuthGuard] },
    { path: 'report', component: ReportComponent, canActivate: [AuthGuard] },
    { path: 'chat', component: ChatComponent, canActivate: [AuthGuard] },
    { path: 'content-calendar', component: ContentCalendarComponent, canActivate: [AuthGuard] },
    { path: 'request-video', component: RequestVideoComponent, canActivate: [AuthGuard] }
]

export const routes: ModuleWithProviders = RouterModule.forRoot(router);
