import { Component, OnInit } from '@angular/core';
import { SlackService } from '../services/slack.service';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import 'rxjs/add/operator/take';
import { Observable } from 'rxjs/Rx';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-request-video',
  templateUrl: './request-video.component.html',
  styleUrls: ['./request-video.component.css']
})
export class RequestVideoComponent implements OnInit {
  user: Observable<any>;
  company: Observable<any>;
  key: string;
  userName: string;
  companyName: string;

  constructor( private afAuth: AngularFireAuth, public db: AngularFireDatabase,
        private slackService:SlackService, private userService:UserService ) {
    this.userService.isLoaded().subscribe( loaded => {
      if ( loaded ){
        this.userService.getCurrentUser().subscribe(
         snapshot => {
           this.userName = snapshot['firstName'] + " " + snapshot['lastName'];
           console.log(this.userName);
           this.userService.getCurrentAccount().subscribe(
            obj => {
              this.companyName = obj['name'];
              console.log(this.companyName);
           });
         });
      }
    });

  }

  ngOnInit() {



  }

  sendRequest(){
    let message = {};
    message['message'] = "Attention: @kyle @brenna "+this.userName + " at " + this.companyName + " is requesting a video";
    this.slackService.sendMessage(message, 'clientvideoupsell');
    alert("Thank you. Someone from JumpCrew will be in touch with you shortly.");
  }



}
