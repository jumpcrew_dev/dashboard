import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { moveIn, fallIn, moveInLeft } from '../router.animations';
import { UserService } from '../services/user.service';
import * as moment from 'moment';
import { DomSanitizer } from '@angular/platform-browser';
import { PostModalComponent } from '../post-modal/post-modal.component';
import { ImageModalComponent } from '../image-modal/image-modal.component';

declare var mixpanel: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  animations: [moveIn(), fallIn(), moveInLeft()],
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild('postModal') postModal: PostModalComponent;
  @ViewChild('imgModal') imgModal: ImageModalComponent;
  key: String;
  user: FirebaseObjectObservable<any>;
  userName: string;
  imagesList: FirebaseListObservable<any>;
  reports: FirebaseListObservable<any>;
  posts: FirebaseListObservable<any>;
  userSubscription: any;
  accountSubscription: any;
  total_followers: number;
  total_new_followers: number;
  total_reach: number;

  sanitize(url:string){
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  editPost(post:any){
    this.postModal.editPost(post);
  }

  showImage(image:any){
    this.imgModal.editImage(image);
  }

  isVideo(imageURL:string){
    return /\.(mp4|mov|m4v)/i.test(imageURL);
  }


  constructor( public db: AngularFireDatabase, private afAuth:AngularFireAuth,
          private userService:UserService, private sanitizer:DomSanitizer ) {
    mixpanel.track("View Uploads");

    let now = moment().utc();
    console.log(now.format());
    this.userService.isLoaded().subscribe(
      loaded => {
        if ( loaded ){
//          console.log("Loaded");
         this.userSubscription = this.userService.getCurrentUser().subscribe(
           snapshot => {
             this.userName = snapshot['firstName'];// + " " + snapshot['lastName'];
              console.log(this.userName);
            this.accountSubscription = this.userService.getCurrentAccount().subscribe(
              obj => {
                   this.key = obj['$key'];
                   console.log(this.key);
                  this.imagesList = this.userService.getUploads();

                   this.reports = this.db.list("/reports/" + this.key,
                    {
                      query: {
                        orderByKey: true,
                        limitToLast: 1,
                      }
                    }
                  );
                  this.reports.subscribe(
                     reports => {
                        if ( reports.length > 0 ){
                          if ( reports[0].total_followers ){
                            this.total_followers = reports[0].total_followers;
                          }
                          if ( reports[0].total_new_followers ){
                            this.total_new_followers = reports[0].total_new_followers;
                          }
                          if ( reports[0].total_reach ){
                            this.total_reach = reports[0].total_reach;
                          }


                        }
                     });

                   this.posts = this.db.list("/companies/" + this.key + "/posts", {
                      query: {
                        orderByChild: 'date',
                        startAt: now.format(),
                        limitToFirst: 4
                      }
                    });
                    //posts.subscribe(



              });


           }
         );
        }
     });
  }



  ngOnInit() {

  }

}
