import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { firebaseConfig } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TitleCasePipe } from '@angular/common';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { EmailComponent } from './email/email.component';
import { SignupComponent } from './signup/signup.component';
import { AuthGuard } from './auth.service';
import { routes } from './app.routes';
import { HomeComponent } from './home/home.component';
import { UploadsComponent } from './uploads/uploads.component';
import { ApproveComponent } from './approve/approve.component';
import { ReportComponent } from './report/report.component';
import { SecureURLPipe } from './secure-url.pipe';
import { OrderByPipe } from './order-by.pipe';
import { Modal } from "ngx-modal";
import { ModalModule } from "ngx-modal";
import { ChatComponent } from './chat/chat.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ContentCalendarComponent } from './content-calendar/content-calendar.component';
import { RequestVideoComponent } from './request-video/request-video.component';
import { PostModalComponent } from './post-modal/post-modal.component';
import { ImageModalComponent } from './image-modal/image-modal.component';

import { EllipsisPipe } from './ellipsis.pipe';
import { SafeHTMLPipe } from './safe-html.pipe';
import { SafeURLPipe } from './safe-url.pipe';
import { KeysPipe } from './keys.pipe';
import { UrlEncodePipe } from './url-encode.pipe';

import { SlackService } from './services/slack.service';
import { FirebaseAdminService } from './services/firebase-admin.service';
import { SociService } from './services/soci.service';
import { UserService } from './services/user.service';
import { SwitchAccountService } from './services/switch-account.service';

import { Angulartics2Module } from 'angulartics2';
import { Angulartics2Mixpanel } from 'angulartics2/mixpanel';

import { SidebarModule } from 'ng-sidebar';
import { NgxPaginationModule } from 'ngx-pagination';
import { ChangeDetectorRef } from '@angular/core';
import { CarouselModule } from "angular2-carousel";
import { ChartModule } from 'angular2-chartjs';

import {MatButtonModule, MatCheckboxModule} from '@angular/material';

import { MatTabsModule } from '@angular/material/tabs';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    routes,
    ModalModule,
    Angulartics2Module,
    SidebarModule.forRoot(),
    NgxPaginationModule,
    BrowserAnimationsModule,
    CarouselModule,
    ChartModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule
  ],
  providers: [ AuthGuard, SlackService, FirebaseAdminService, SociService, UserService, SwitchAccountService ],
  declarations: [ AppComponent, LoginComponent, EmailComponent, SignupComponent, HomeComponent, UploadsComponent, ApproveComponent, ReportComponent, SafeURLPipe, SecureURLPipe, OrderByPipe, ChatComponent, PasswordResetComponent, ForgotPasswordComponent, EllipsisPipe, SafeHTMLPipe, ContentCalendarComponent, RequestVideoComponent, PostModalComponent, ImageModalComponent, KeysPipe, UrlEncodePipe ],
  bootstrap: [ AppComponent ]
})export class AppModule {}
