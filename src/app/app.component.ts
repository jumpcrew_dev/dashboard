import { Component } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router, RouterLinkActive, ActivatedRoute } from '@angular/router';
import { Modal, ModalModule } from "ngx-modal";
import { ViewChild }  from '@angular/core';
import { SlackService } from './services/slack.service';
import { FirebaseAdminService } from './services/firebase-admin.service';
import { UserService } from './services/user.service';
import { environment } from '../environments/environment';
import * as firebase from 'firebase';
import 'rxjs/add/operator/take';
import { Subject, Observable } from 'rxjs/Rx';
declare var typeformEmbed: any;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent {
  @ViewChild('upsellModal') upsellModal: any;
  user: FirebaseObjectObservable<any>;
  company: FirebaseObjectObservable<any>;
  uid: string;
  companyKey: string;
  companyKeys: string[];
  soci_key: string;
  soci_url: string;
  email: string;
  uberall_account: string;
  account_type: any;
  account_types: any;
  account_type_str: string;
  userName: string;
  firstName: string;
  lastName: string;
  companyName: string;
  server_url: string;
  lastNPS: string;
  soci_id: string;
  _opened: boolean;
  userCompanies: FirebaseListObservable<any>;
  companies: any[];
  configSubscription: any;
  errorText:string;

  title = 'JumpCrew Client Portal';

  constructor( public db: AngularFireDatabase, public afAuth:AngularFireAuth, private router: Router, private route:ActivatedRoute,
    private slackService:SlackService, private firebaseAdmin:FirebaseAdminService, private userService:UserService ) {

  }

  switchAccount(key:string){
    //console.log("Switching to "+key);
    this.userService.switchAccount(key);
  }

  ngOnDestroy(){
    this.configSubscription.unsubscribe();
  }

  ngOnInit(){
    this.server_url = environment.server_url;
    this.afAuth.authState.subscribe(auth => {
     if ( auth ){
       this.configSubscription = this.db.object("/app_config/").subscribe(
         config => {
           this.soci_url = config.soci_url;
           this.account_types = config.account_types;
          }
       );
      }
    });
    this._opened = false;
    this.userService.isLoaded().subscribe( loaded => {
    if ( loaded ){
     this.userService.getCurrentUser().subscribe(
       snapshot => {
         console.log("User object loaded");
         this.firstName = snapshot['firstName'];
         this.lastName = snapshot['lastName'];
         this.userName = snapshot['firstName'] + " " + snapshot['lastName'];
         this.soci_key = snapshot['soci_key'];
         this.email = snapshot['email'];
         this.lastNPS = snapshot['lastNPS'];
         this.uid = snapshot['$key'];
         this.companyKeys = Object.keys(snapshot['companies']);
          this.companies = [];
             if ( this.companyKeys.length != this.companies.length ){
              //console.log("Repopulating companies array");
               this.companyKeys.forEach(
                  key => {
                    //console.log(key);
                    this.db.object("/companies/"+key).take(1).subscribe(
                      obj => {
                        //console.log("Adding "+obj['$key']);
                        var found = this.companies.find(function(element) {
                          return element['$key'] == obj['$key'];
                        });
                        if ( !found ){
                         this.companies.push(obj);
                         this.companies.sort((a:any, b:any) => {
                           //console.log("Comparing "+a+" and "+b);
                           return a.name.localeCompare(b.name);
                         });
                        }

                      });

                  }
                );
              }

        /*console.log("Token: ");
        firebase.auth().currentUser.getToken()
          .then(function(idToken){
            console.log(idToken);
          });*/

         this.company = this.userService.getCurrentAccount();
         this.company.subscribe(obj => {
           console.log("company obj loaded");
           console.log(obj);
            if ( obj['uberall_account'] ){
              this.uberall_account = obj['uberall_account'];
             }
             this.account_type = obj['account_type'];
             this.account_type_str = Object.keys(obj['account_type'])[0];
             this.companyName = obj['companyName'];
             this.soci_id = obj['soci_id'];
             this.companyKey = obj['$key'];

             var today = new Date();
             if ( this.lastNPS ){
               var date = new Date(this.lastNPS);
               date.setDate(date.getDate()+30);
               if ( date < today ){
                this.showNPSForm();
                //console.log("Doing the update here");
                this.userService.updateUser({ "lastNPS": today.toISOString() });
               }
             }
             else {
              this.firebaseAdmin.getUserCreatedAt(this.uid).subscribe(
                (date) => {
                  //console.log("Created: " + date + ". Today : " + today);
                  date.setDate(date.getDate()+30);
                  if ( date < today ){
                   this.showNPSForm();
                   //console.log("Doing the update there");
                   this.userService.updateUser({ "lastNPS": today.toISOString() });
                  }
                }
              );
             }

         });


       });
      }
    });
  }
  interrupt(block:boolean, event:any){
    //console.log("In interrupt");
    //console.log(event);
    if ( block ){
      //this.upsellModal.open();
      event.stopPropagation();
      alert("Your account doesn't have this feature. Contact your account manager to find out how you can upgrade.");

      return false;
    }

  }

  openUpsellModal(){
    if ( this.account_type_str == 'spark' || this.account_type_str == 'ignite' ){
      this.upsellModal.open();
    }
    else {
      return false;
    }
  }

  upsell(){
    //console.log(this.companyName);
    let message = {}
    message['message'] = this.userName + " at " + this.companyName + " is interested in being upsold.";
    this.slackService.sendMessage(message, "upsell_leads");
  }

  logout() {
     this.afAuth.auth.signOut();
     //console.log('logged out');
     this.router.navigateByUrl('/login');
  }

  showNPSForm(){
    if ( typeformEmbed ){
     var qstr = "email="+this.email;
     qstr += "&first_name="+this.firstName;
     qstr += "&last_name="+this.lastName;
     qstr += "&source=dashboard";
     qstr += "&account_name="+this.companyName;
     qstr += "&account_id="+this.companyKey;
      if ( this.soci_id ){
        qstr += "&soci_account_id="+this.soci_id;
      }
      if ( this.uberall_account ){
        qstr += "&uberall_account_id="+this.uberall_account;
      }



     typeformEmbed.makePopup('https://jumpcrew.typeform.com/to/oyBmpX?'+qstr, {
       mode: 'drawer_left',
       autoOpen: true,
       autoClose: 3
     });
    }
  }


}

