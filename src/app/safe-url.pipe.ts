import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Pipe({
  name: 'safeURLPipe'
})
export class SafeURLPipe implements PipeTransform {
  constructor( public sanitizer:DomSanitizer ){
    //this.sanitizer = sanitizer;
  }

  transform(url) {
        //console.log(url);
        var myUrl= this.sanitizer.bypassSecurityTrustResourceUrl(url);
        console.log(myUrl);
        return myUrl;
  }
}
