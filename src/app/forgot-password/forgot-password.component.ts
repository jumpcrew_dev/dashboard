import { Component, OnInit } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { moveIn, fallIn } from '../router.animations';
import * as firebase from 'firebase';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
  animations: [moveIn(), fallIn()],
  host: {'[@moveIn]': ''}
})
export class ForgotPasswordComponent implements OnInit {

  sent: boolean;
  error: String;

  constructor( public db: AngularFireDatabase, private afAuth:AngularFireAuth, private router: Router ) {}

  ngOnInit() {

  }

  onSubmit(formData) {
    if(formData.valid) {
      //console.log(formData.value);
      firebase.auth().sendPasswordResetEmail(formData.value.email)
        .then( result => {
              //console.log(result);
              this.sent = true;
             },
             error => {
               console.log(error);
               if ( error['code'] == 'auth/user-not-found' ){
                this.error = "Looks like we don't have a user with that email address. Are you sure you typed it correctly?";
               }
             });
      //this.router.navigate([ '/login-email' ]);

    }
  }
}
