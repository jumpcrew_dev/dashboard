import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { SociService } from './soci.service';
import { ChangeDetectorRef } from '@angular/core';
import { ApplicationRef } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs/Rx';

@Injectable()
export class UserService {
  user: any;
  userObservable: FirebaseObjectObservable<any>;
  userSubject: FirebaseObjectObservable<any>;
  accountSubject: FirebaseObjectObservable<any>;
  userSubscription: any;
  userCompanies: any[];
  key: string;
  currentAccount: FirebaseObjectObservable<any>;
  accountSubscription: any;
  companyKeys: string[];
  loaded: Subject<boolean> = new BehaviorSubject(false);
  uploads: FirebaseListObservable<any>;

  isLoaded() : Observable<boolean>{
    return this.loaded.asObservable();
  }

  constructor( private appRef:ApplicationRef, private sociService:SociService, private afAuth: AngularFireAuth, private db: AngularFireDatabase, private router: Router ) {
    //console.log("In UserService constructor");
    this.userSubject = new FirebaseObjectObservable<any>();
    this.accountSubject = new FirebaseObjectObservable<any>();
    this.afAuth.authState.subscribe(auth => {
       if(auth) {
       //console.log("caught AuthState in UserService constructor ");
         //console.log("Auth obj: ");
         //console.log(auth);
         //setTimeout( () => {
         this.userObservable = this.db.object("/users/"+auth.uid);
         //this.userObservable.subscribe(this.userSubject);
         this.userSubscription = this.userObservable.subscribe(
           user => {
             this.user = user;
             console.log(user);
             this.companyKeys = Object.keys(user.companies);
             if ( user.selectedCompany ){
              this.key = user.selectedCompany;
             }
             else {
              this.key = this.companyKeys[0];
             }
              //console.log(this.key);
              this.uploads = this.db.list('/companies/'+this.key+'/images/', {
                     query: {
                      orderByChild: "deleted",
                      equalTo: null
                    }
                   });
               this.currentAccount = this.db.object("/companies/"+this.key) as FirebaseObjectObservable<any>;
               this.accountSubscription = this.currentAccount.take(1).subscribe(
                 data =>{
                  //console.log("loaded account");
                  if ( data.account_type.engage || data.account_type.premier || data.soci_id ){
                    console.log("Fetching pending posts from constructor for "+ this.key);
                    this.sociService.fetchPendingPosts(this.key);
                    // TODO: we won't need to do this if we add a cron job to run this report nightly.
                    //      but it might be good insurance, and it won't affect the page-loading time
                    this.sociService.runReport(this.key);

                   }
                   this.loaded.next(true);

                 }
               );

             if ( user.deleted  ){
              console.log("Deleting user account");
              var fuser = firebase.auth().currentUser;

              fuser.delete().then(function() {
                // User deleted.
              }).catch(function(error) {
                // An error happened.
                 console.log("Error deleting user");
                 console.log(error);
              });
               this.router.navigate([ '/login' ]);
               this.afAuth.auth.signOut();
             }


            }
          );


           //  }, 2000);
       }
       else {
        //console.log("Should probably unsubscribe here");
        if ( this.userSubscription ){
          this.userSubscription.unsubscribe();
          //this.userObservable.complete();
          //console.log("Unsubscribed from user object");
        }
        if ( this.accountSubscription ){
          this.accountSubscription.unsubscribe();
          //this.currentAccount.complete();
          //console.log("Unsubscribed from account object");
        }
       }
     });

  }

  // getCurrentUser() : FirebaseObjectObservable<any> {
//     return this.userObservable  ;
//   }

  getCurrentUser() : FirebaseObjectObservable<any> {
    return this.userObservable;
  }

  getCurrentAccount(): FirebaseObjectObservable<any>{
    return this.currentAccount;
  }

  getCurrentAccountKey():string{
    return this.key;
  }

  getUploads():FirebaseListObservable<any>{
    return this.uploads;
  }

  updateUser( obj:any ){
    this.userObservable.update(obj);
  }

  switchAccount(key:string){
    //console.log("In switchAccount, "+key);
    this.key = key;
    if ( this.accountSubscription ){
      this.accountSubscription.unsubscribe();
    }
    this.currentAccount = this.db.object("/companies/"+this.key);
    this.userObservable.update({ "selectedCompany": this.key});
    //location.reload();
    //this.cdRef.detectChanges();
    this.appRef.tick();
  }
}
