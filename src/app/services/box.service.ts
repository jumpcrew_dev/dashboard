import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, ResponseContentType, URLSearchParams } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { environment } from '../../environments/environment';
import { Observable } from "rxjs/Rx";

const box_url = "https://upload.box.com/api/2.0/files/content";

@Injectable()
export class BoxService {
  box_group_id: string;
  box_token: string;
  config: any;


  constructor( public db: AngularFireDatabase, private afAuth:AngularFireAuth, private http: Http ) {
    this.config = this.db.object("/app_config/");
    this.config.subscribe(
      config => {

        this.box_group_id = config.box_group_id;
        this.box_token = config.box_token;

        }
      );

  }

  public getThumbnail(id:string, callback){
      var min = 160;
      var max = 256;

      var link = "https://api.box.com/2.0/files/"+id+"/thumbnail.png";
      link += "?min_height="+min+"&min_width="+min;
      link += "&max_height="+max+"&max_width="+max;

    let headers = new Headers();
    headers.append('Authorization', 'Bearer '+ this.box_token);

    let options = new RequestOptions({
      headers: headers,
      responseType: ResponseContentType.Blob
    });



       this.http.get(link, options)
//
//         fetch(link, {
//            headers: {
//                Authorization: 'Bearer '+this.box_token
//            }
//         })
//         .map(res => {
//             return new Blob([res._body], {
//                 type: res.headers.get("Content-Type")
//             });
//         })
        .subscribe((response) => {
          //console.log(response);
          if ( response.status == 202 ){
            var wait = 10;
            console.log("Thumbnail not ready yet. Trying again in " + wait + " seconds");
            window.setTimeout( () => {
                this.getThumbnail(id, callback);
              }, wait * 1000);
          }
          else if ( response.status == 200 ){
            console.log(response);
            var blob = new Blob([(response as any)._body], {
                 type: 'image/png'//response.headers.get("Content-Type")
             });
            /*response.blob()
              .subscribe(
                (blob) => {*/
                  var reader = new (window as any).FileReader();
                  reader.onloadend = function () {
                    var base64data = reader.result;
                    //console.log(base64data);
                    callback(base64data);
                  }
                  reader.readAsDataURL(blob);
              //  }
              //);
          }
          }
        );

    }

  public uploadFile( file:File, folder_id:string ){
    let formData:FormData = new FormData();
    let parent = {
      "id": folder_id
    };
    let attributes = {
      "parent" : parent,
      "name": new Date().getTime()+file.name
    };
    formData.append('attributes', JSON.stringify(attributes));
    formData.append('file', file, file.name);

    let headers = new Headers();
    /** No need to include Content-Type in Angular 4 */
    //headers.append('Content-Type', 'multipart/form-data');
    headers.append('Authorization', 'Bearer '+this.box_token);
    let options = new RequestOptions({ headers: headers });

    return this.http.post(box_url, formData, options)
        .map(res => res.json())
        .catch(error => Observable.throw(error));


  }

  public shareFile( id:string ){
    var link = "https://api.box.com/2.0/files/"+id+"?fields=shared_link";
    var data = {
      "shared_link": {
        "access" : "open",
        "permissions": {
          "can_download": true
        }
      }
    };

/*        $http(req).then(function(resp){
          console.log("Share link result: ");
          console.log(resp.data.shared_link);
            var downloadURL = resp.data.shared_link.download_url;
            callback(downloadURL);
        });
  */

    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Authorization', 'Bearer '+ this.box_token);

    let options = new RequestOptions({
      headers: headers
    });

    return this.http.put(link, JSON.stringify(data), options)
      .map( response => JSON.parse(response['_body']) );

  }

  public shareFolder( id: string ){
    let link = "https://api.box.com/2.0/collaborations?notify=true";
    var data = {
      "item" : {
        "type": "folder",
        "id" : id
      },
      "accessible_by": {
        "id": this.box_group_id,
        "type": "group"
      },
      "role": "editor"
    };

    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Authorization', 'Bearer '+ this.box_token);

    let options = new RequestOptions({
      headers: headers
    });

    this.http.post(link, JSON.stringify(data), options)
      .map( response => JSON.parse(response['_body']) )
      .subscribe(
        (body) => {
         console.log(body);

        },
        (err) =>{
         console.log("Error: ");
         console.log(err);

      });


  }
  public createFolder( name: string, company_key: string ){
      console.log("Creating folder for "+name);
      let link = "https://api.box.com/2.0/folders";
      let headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Authorization', 'Bearer '+ this.box_token);

      let options = new RequestOptions({
        headers: headers
      });

      let body = JSON.stringify(
        {
         "name": name,
         "parent": {
            "id": 0
         }
        }
      );

      //console.log(req);
      this.http.post(link, body, options)
      .map( response => JSON.parse(response['_body']) )
      .subscribe(
        (body) => {
         console.log(body);
         //console.log(resp['_body']);
         //var body = JSON.parse(resp['_body']);
         //console.log(body);
         if ( body.id ){
            console.log("Saving box folder ID to db");
            this.db.object('/companies/'+company_key).update({ box_folder_id: body.id });
          }
         this.shareFolder(body.id);
        },
        (err) =>{
         console.log("Error: ");
         console.log(err);
          var body = JSON.parse(err['_body']);
          console.log(body);
         var conflicts = body.context_info.conflicts;
         if ( conflicts ){
           console.log("Existing folder ID: "+conflicts[0].id);
           this.db.object('/companies/'+company_key).update({ box_folder_id: conflicts[0].id });
           this.shareFolder(conflicts[0].id);
         }
      });
    }

}
