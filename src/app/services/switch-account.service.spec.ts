/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SwitchAccountService } from './switch-account.service';

describe('SwitchAccountService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SwitchAccountService]
    });
  });

  it('should ...', inject([SwitchAccountService], (service: SwitchAccountService) => {
    expect(service).toBeTruthy();
  }));
});
