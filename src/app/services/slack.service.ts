import { Injectable } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

@Injectable()
export class SlackService {
  slackToken: string;

  constructor( public db: AngularFireDatabase, private afAuth:AngularFireAuth, private http:Http) {
    this.afAuth.authState.subscribe(auth => {
     if ( auth ){
      //console.log("Attempting access");
      this.db.object("/app_config/").subscribe(
        config => {
           this.slackToken = config.slack_token;
         }
      );
    }
    })
  }

  public sendMessage(message:any, channel_name:string): void{

    let link = "https://slack.com/api/chat.postMessage";
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers });

    let fd = "token=" + this.slackToken + "&" +
            "channel=" + channel_name + "&" +
            "text=" + message['message']  + "&" +
            "link_names=true&" +
            "attachments="+ encodeURIComponent(JSON.stringify(message['attachments']));

    this.http.post(link, fd, options)
     .map( response => response.json() )
     .catch( this.handleError )
     .subscribe(
       result => {
          console.log(result);
         }
       );

  }

  private handleError (error: Response | any) {
    console.log("In handleError");
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Promise.reject(errMsg);
  }
}
