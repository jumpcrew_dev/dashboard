/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FirebaseAdminService } from './firebase-admin.service';

describe('FirebaseAdminService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FirebaseAdminService]
    });
  });

  it('should ...', inject([FirebaseAdminService], (service: FirebaseAdminService) => {
    expect(service).toBeTruthy();
  }));
});
