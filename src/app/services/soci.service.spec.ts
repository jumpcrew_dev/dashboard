/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SociService } from './soci.service';

describe('SociService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SociService]
    });
  });

  it('should ...', inject([SociService], (service: SociService) => {
    expect(service).toBeTruthy();
  }));
});
