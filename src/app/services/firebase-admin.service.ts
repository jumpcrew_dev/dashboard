import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable } from "rxjs/Rx";
import { Http, Response, Headers, RequestOptions } from '@angular/http';


@Injectable()
export class FirebaseAdminService {

  constructor( private http:Http ) { }

  getUserCreatedAt( uid:string ){
    let link = environment.admin_url;
    link += "&uid="+uid;
    link += "&action=get";

    return this.http.get(link, null).
      map(response => new Date(Number(response.json()['_data']['createdAt'])))
      .catch(error => Observable.throw(error));

  }

  getMagicURL( path:string ){
    let link = environment.gae_url + path;
    return this.http.get(link, null)
      .map(response => response['_body'])
      .catch(error => Observable.throw(error));

  }

}
