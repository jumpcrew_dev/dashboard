import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { environment } from '../../environments/environment';
import * as moment from 'moment';
import { ReplaySubject, Observable } from 'rxjs/Rx';

@Injectable()
export class SociService {
  soci_api_key: string;
  config: FirebaseObjectObservable<any>;
  pending_posts: FirebaseListObservable<any>;
  proxy: string;
  newCount: number = 0;
  approvedCount: number = 0;
  rejectedCount: number = 0;
  posts: FirebaseListObservable<any>;
  soci_id: string;
  configSubscription: any;

  getNewCount():Observable<number>{
    return Observable.of(this.newCount);
  }

  getApprovedCount():Observable<number>{
    return Observable.of(this.approvedCount);
  }

  getRejectedCount():Observable<number>{
    return Observable.of(this.rejectedCount);
  }

  ngOnDestroy(){
    console.log("called destroy");
      this.configSubscription.unsubscribe();
  }

  constructor( private http:Http, private db:AngularFireDatabase, private afAuth: AngularFireAuth ) {
//    this.pending_posts = new ReplaySubject<FirebaseListObservable<any>>();
    this.soci_api_key = "qjyfA3KLbG7NOfd8or8nnmB7l8K0Y7KoqiD1loMw";
    this.afAuth.authState.subscribe(auth => {
     if ( auth ){
      //console.log("Attempting access");
      this.config = this.db.object("/app_config/");
      this.configSubscription = this.config.subscribe(
        config => {
            //console.log("Config object", config);
            this.soci_api_key = config.soci_api_key;
          }
        );
      }
    });
    this.proxy = environment.server_url+"/proxy.php?dst=";
  }

  getPendingPosts():FirebaseListObservable<any>{
    return this.posts;
  }

  runReport(account_id: string){
    var link = environment.jumpcrew_api_base + "/clients/" + account_id + "/report";
    //console.log(link);
    //console.log(Date.now()/1000);
    this.http.get(link).
    map( response => response.json() )
    .subscribe(
         (body) => {
            //console.log("Came back from API call");
            //console.log(Date.now()/1000);
            //console.log(body);
          }
        );
  }

  fetchPendingPosts(account_id: string){
    //console.log("In fetchPendingPosts");
    var link = environment.jumpcrew_api_base + "/clients/" + account_id + "/posts";
    //console.log(link);
    //console.log(Date.now()/1000);
    this.http.get(link).
    map( response => response.json() )
    .subscribe(
         (body) => {
            //console.log("Came back from API call");
            //console.log(Date.now()/1000);
            //console.log(body);
          }
        );


    var now = moment();
    this.posts = this.db.list("/companies/" + account_id + "/posts", {
       query: {
         orderByChild: 'date',
         startAt: now.format('YYYY-MM-DD')
       }
     });
//    this.posts.subscribe(this.pending_posts);
    this.posts
    .subscribe( posts => {
         console.log("Fetched Pending Posts, processing");
         this.newCount = 0;
         this.approvedCount = 0;
         this.rejectedCount = 0;
      if ( posts && typeof posts == "object" ){
       posts.forEach( post => {
          // the API should be filtering these out now
          if ( !post.manager_approved || post.manager_approved == "0" ){
            return;
          }
          //var date = moment(post.schedule);
          //var dateStr = date.format('YYYY-MM-DD');

          // need to update instead of appending
          //post.date = date;

          switch (post.status){
            case "new":
              if ( !post.expired ){
                this.newCount++;
              }
              break;
            case "rejected":
              if ( !post.expired ){
                this.rejectedCount++;
              }
              break;
            case "approved":
              if ( !post.expired ){
                this.approvedCount++;
              }
              break;
          }
       });
       }
      });
  }

  approveOrReject(account_id:string, id:string, approved:number, message:string){


      var link = environment.jumpcrew_api_base + "/clients/" + account_id + "/posts/" +id;
      var body = { "status" : ( approved == 1 ? "approved" : "rejected"), "rejection_message": message };

      return this.http.patch(link, body).
      map( response => response.json() );
      /*var link = "https://app.meetsoci.com/api/promote/"+project_id+"/customer_approve";

      link += "?api_key="+this.soci_api_key;

      link += "&message_id="+id;
      link += "&customer_approved="+approved;
      link += "&project_id="+project_id;

      if ( message ){
       link += "&message="+encodeURIComponent(message);
      }

      console.log(link);

      var headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
      var options = new RequestOptions({ headers: headers });

      return this.http.get(this.proxy+encodeURIComponent(link), options)
      .map( response => response.json() );*/

  };

  approve(account_id:string, post:any){
    if ( post.customer_approved == 0 ){
      this.newCount--;
      //Soci.decrementSummaryCount(post.dateStr, 'new');
    }
    else {
      this.rejectedCount--;
      //Soci.decrementSummaryCount(post.dateStr, 'rejected');
    }
    this.approvedCount++;
    //Soci.incrementSummaryCount(post.dateStr, 'approved');
    post.customer_approved = 1;
    post.status = "approved";
    return this.approveOrReject(account_id, post.id, 1, "");
  };

  reject(account_id:string, post: any, message: string){
    if ( post.customer_approved == 0 ){
       this.newCount--;
       //Soci.decrementSummaryCount(post.dateStr, 'new');
     }
     else {
       this.approvedCount--;
       //Soci.decrementSummaryCount(post.dateStr, 'approved');
     }
     this.rejectedCount++;
     //Soci.incrementSummaryCount(post.dateStr, 'rejected');
     post.customer_approved = -1;
     post.status = "rejected";

    return this.approveOrReject(account_id, post.id, -1, message);
  };



}
