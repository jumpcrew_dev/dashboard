import { Component, OnInit } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { ActivatedRoute, Router } from '@angular/router';
import { moveIn, fallIn } from '../router.animations';
import * as firebase from 'firebase';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.css'],
  animations: [moveIn(), fallIn()],
  host: {'[@moveIn]': ''}
})
export class PasswordResetComponent implements OnInit {
  code: string;
  error: string;
  email: string;
  sent: boolean;

  constructor( public db: AngularFireDatabase, private afAuth:AngularFireAuth, private route: ActivatedRoute, private router: Router ) {}

  ngOnInit() {
    this.code = this.route.snapshot.params['code'];
    if ( this.code ){
      console.log(this.code);
      firebase.auth().verifyPasswordResetCode(this.code)
        .then(  email => {
          this.email = email;
          console.log(this.email);
        },
        error => {

        });

    }
  }

  onSubmit(formData) {
    if(formData.valid) {
      //console.log(formData.value);
      firebase.auth().confirmPasswordReset(this.code, formData.value.password)
        .then( result => {
              //console.log(result);
              this.sent = true;
             },
             error => {
               //console.log(error);
                this.error = error['message'];
             });

      //this.router.navigate([ '/login-email' ]);

    }
  }

}
