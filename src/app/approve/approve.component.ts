import { Component, OnInit } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserService } from '../services/user.service';
import { SociService } from '../services/soci.service';
import { Subject, Observable } from 'rxjs/Rx';

declare var mixpanel: any;

@Component({
  selector: 'app-approve',
  templateUrl: './approve.component.html',
  styleUrls: ['./approve.component.css']
})
export class ApproveComponent implements OnInit {
  user: FirebaseObjectObservable<any>;
  key: string;
  company: FirebaseObjectObservable<any>;
  url: String = "";
  filterTxt: string = "approved";
  posts: Observable<FirebaseListObservable<any[]>>;
  soci_id: string;
  reason: string;
  selectedPost: any;

  constructor( public db: AngularFireDatabase,
               public afAuth:AngularFireAuth,
               public userService:UserService,
               public sociService:SociService ) { }

  ngOnInit() {

    window.scrollTo(0, 0);
    this.userService.isLoaded().subscribe( loaded => {
      if ( loaded ){
          this.userService.getCurrentUser().subscribe(
        snapshot => {
          this.company = this.userService.getCurrentAccount();
          this.company.subscribe(
            obj => {
              //console.log("Company obj"+obj);
              this.key = obj['$key'];
              this.url = "https://app.meetsoci.com/approve?id="+obj['soci_id']+"&token="+obj['soci_token'];
              //console.log(this.url);
              this.posts = this.sociService.getPendingPosts();
              console.log(this.posts);
              this.soci_id = obj['soci_id'];
              this.sociService.getNewCount().take(1).subscribe(
                newCount => {
                  console.log("New count: "+newCount);
                  if ( newCount > 0 ){
                    this.filterTxt = 'new';
                  }
                }
              );
            }
            );
        }
      );
    }
    });

  }

  ngAfterViewInit(){
    mixpanel.track("Approve Content Screen");
  }

  filter(str:string){
    if ( !this.filterTxt ){
      return true;
    }
    if ( str ){
      if ( str.toLowerCase().indexOf(this.filterTxt.toLowerCase()) >= 0 ){
        return true;
      }
    }
    return false;
  }

  approve(post:any){
    //console.log("Approve");
    //console.log(post);
    this.sociService.approve(this.key, post)
      .subscribe( (res) => {
        //console.log("Result of approve", res);
      });

  }

  modify():boolean{
    if ( !this.reason || this.reason == "" ){
      alert("Please let us know what you would like us to change");
      return false;
    }
    this.selectedPost.rejection_message = this.reason;
    this.sociService.reject(this.key, this.selectedPost, this.reason)
      .subscribe( (res) => {
        //console.log("Result of reject", res);
        //postDetails.close();
        //return true;
      });
    return true;
  }

}
