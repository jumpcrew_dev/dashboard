import { Component, OnInit, ElementRef, ViewChild, AfterViewChecked } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { NgModule } from '@angular/core';

//import { Angulartics2, Angulartics2Module } from 'angulartics2';
//import { Angulartics2Mixpanel } from 'angulartics2/mixpanel';

import { UserService } from '../services/user.service';
import { SlackService } from '../services/slack.service';
declare var mixpanel: any;

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit{
  message: String;
  slackToken: String;
  slackUser: String;
  latest: String;
  messages: Array<Object>;
  company: FirebaseObjectObservable<any>;
  config: FirebaseObjectObservable<any>;
  user: FirebaseObjectObservable<any>;
  key: string;
  channel_id: string;
  channel_name: string;
  userName: string;
  link_patt: RegExp;
  intervalId: number;

  constructor( public db: AngularFireDatabase, private afAuth:AngularFireAuth, private http: Http,
    public slackService:SlackService, private userService:UserService ) {
    this.db.object("/app_config/").subscribe(
      config => {
         console.log(config);
         this.slackToken = config.slack_token;
         console.log(this.slackToken);
       }
    );

    this.link_patt = new RegExp(/<(http[^>]*)>/i);
  }


  scrollToBottom(): void {
    //console.log("Scroll to Bottom");
        try {
            var end = document.getElementById("end")
            if ( end ){
              end.scrollIntoView(false);
            }
        }
        catch(err) {
          console.log(err);
        }
    }

  sendMessage(): void{
    mixpanel.track("Sent Message");

    let message = {};
    message['message'] = this.userName ;
    message['attachments'] = [{'text': this.message}];

    this.slackService.sendMessage(message, this.channel_id);
    this.message = "";
  }

  ngOnInit() {
      mixpanel.track("Entered Chat Page");

  this.userService.isLoaded().subscribe( loaded => {
    if ( loaded ){

      this.userService.getCurrentUser().subscribe(
       snapshot => {
        console.log("Reloading chat");
        this.messages = new Array<Object>();
        if ( this.intervalId ){
          console.log("Clearing old timer");
          clearInterval(this.intervalId);
        }
        this.latest = ""+0;
         this.userName = snapshot['firstName'] + " " + snapshot['lastName'];
         this.userService.getCurrentAccount().subscribe(
           obj => {
            this.channel_id = obj['channel_id'];
            this.channel_name = obj['channel_name'];


    this.db.object("/slack_channels/"+this.channel_id+"/latest").subscribe(
     result => {
    console.log("Loading everything after "+this.latest);
    let link = "https://slack.com/api/channels.history";
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers });

    let fd = "token="+this.slackToken+"&channel="+this.channel_id+"&oldest="+this.latest;
    //console.log(fd);


    this.http.post(link, fd, options)
      .map(response => response.json() )
      .catch(this.handleError).subscribe(
        result => {
          //console.log(result);
          if ( result.messages && result.messages.length > 0 ){
          this.latest = result.messages[0].ts;
          //console.log(this.latest);
          result.messages.forEach( message => {
            //console.log(message);
            if ( !message.subtype || message.subtype == 'bot_message' ){

            let msg = {};
            msg['ts'] = message.ts;
            msg['date'] = new Date( message.ts * 1000 );

            if ( message.user != this.slackUser ){
               msg['text'] = message.text;
               msg['user'] = "JumpCrew Account Manager";
               msg['manager'] = true;

              /*if ( $scope.slack_users.hasOwnProperty(value.user) ){
                msg.user = $scope.slack_users[value.user];
              }
              else {
               Slack.getUserInfo( value.user ).then(function(resp){
                 //console.log("Called getUserInfo for " + value.user);
                 //console.log(resp.data);
                 msg.user = resp.data.user.profile.real_name;
                 if ( !msg.user || msg.user == "" ){
                   msg.user = resp.data.user.name;
                 }
                 $scope.slack_users[value.user] = msg.user;
               });
               }*/
            }
            else if ( message.attachments && message.attachments[0].text){
              msg['user'] = message.text;
              msg['text'] = message.attachments[0].text;
            }
            else {
              return;
            }
            msg['text'] = this.convertEmojis(msg['text']);
            this.messages.push(msg);


          }

        });
        setTimeout(()=> {
            this.scrollToBottom();
          }, 500);
        this.messages.sort((a, b) => {
           if (a['ts'] < b['ts']) return -1;
           else if (a['ts'] > b['ts']) return 1;
           else return 0;
         });

        }
      });
    });

}
         );
       });

       }
    });

  }

  private convertEmojis(txt:string){
    if ( txt && txt != "" ){
      txt = txt.replace(/:skin-tone-[^:]:/g, "");
      txt = txt.replace(/:\+1:/g,":--1:");
      return txt.replace(/:([-a-z0-9_\+]+):/g, "<i class=\"em em-$1\"></i>");
    }
    return "";
  };

  private fixLinks(text:string){
    text = text.replace(this.link_patt, "$1");
    return text;
  };


  private handleError (error: Response | any) {
    console.log("In handleError");
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Promise.reject(errMsg);
  }

}
