import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import * as firebase from 'firebase';

import { UserService } from './services/user.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor( public db: AngularFireDatabase, private afAuth:AngularFireAuth, private router: Router, private userService:UserService) {}

    canActivate(): Observable<boolean> {
      return Observable.from(this.afAuth.authState)
        .take(1)
        .map(state => !!state)
        .do(authenticated => {
          console.log("In authGuard");
          setTimeout( () => {
          if (!authenticated) {
           console.log("Routing to login");
           this.router.navigate([ '/login' ]);
          }
          else {
            return true;
          }
          }, 3000);
      })
    }


}
