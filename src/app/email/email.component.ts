import { Component, OnInit } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { moveIn, fallIn } from '../router.animations';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css'],
  animations: [moveIn(), fallIn()],
  host: {'[@moveIn]': ''}
})
export class EmailComponent implements OnInit {
  state: string = '';
    error: any;
    email:string;
    password: string;

    constructor( public db: AngularFireDatabase, private afAuth:AngularFireAuth, private router: Router) {
      this.afAuth.authState.subscribe(auth => {
        if(auth) {
          //console.log("auth");
          this.router.navigateByUrl('/home');
        }
      });
    }


  onSubmit(formData) {
    if(formData.valid) {
      //console.log(formData.value);
      this.afAuth.auth.signInWithEmailAndPassword(formData.value.email, formData.value.password
      /*{
        provider: AuthProviders.Password,
        method: AuthMethods.Password,
      }*/
      ).then(
        (success) => {
        //console.log(success);
        //this.router.navigate(['/approve']);
        setTimeout(() => {
          //console.log("Routing to uploads");
          this.router.navigateByUrl('/home');
          },1000);
      }).catch(
        (err) => {
        console.log(err);
        this.error = err;
      })
    }
  }

  ngOnInit() {
  }
}
