import { Component, OnInit, ViewChild } from '@angular/core';
declare var mixpanel: any;
import { UserService } from '../services/user.service'

@Component({
  selector: 'image-modal',
  templateUrl: './image-modal.component.html',
  styleUrls: ['./image-modal.component.css']
})
export class ImageModalComponent implements OnInit {
	image: any;
	description: string;
	errorText: string;
	@ViewChild('editModal') editModal: any;

  	constructor( private userService:UserService ) { }

  	ngOnInit() {

  	}

	editImage(image){
	    this.image = image;
	    this.editModal.open();
	}

	saveChanges(){
	    var textField = (<HTMLInputElement>document.getElementById('description'));
	    console.log(textField);
	    this.description = textField.value;
	    this.image['description'] = this.description;
	    this.userService.getUploads().update(this.image['$key'], {"description": this.description});
	    /*.subscribe( (result) => {
	        textField.value = "";
	      });*/
	        mixpanel.track("Edited Image Description");

	    return true;
	}


  isVideo(imageURL:string){
    return /\.(mp4|mov|m4v)/i.test(imageURL);
  }


}
