import { NewdashPage } from './app.po';

describe('newdash App', function() {
  let page: NewdashPage;

  beforeEach(() => {
    page = new NewdashPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
